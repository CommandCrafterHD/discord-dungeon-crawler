import datetime
from discord import Embed, Colour as Color, Status
from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog
from time import gmtime, strftime
from files import colors
from files.utility import get_server

class SystemCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @Cog.listener()
    async def on_ready(self):
        print(f"{colors.YELLOW}BOT STARTED!{colors.END}")
        print("--"*20)
        print(f"{colors.CYAN}Name{colors.END}: {colors.YELLOW}{self.bot.user.name}{colors.END}")
        print(f"{colors.CYAN}ID{colors.END}: {colors.YELLOW}{self.bot.user.id}{colors.END}")
        print("--" * 20)
        for i in self.bot.guilds:
            get_server(i.id, add_if_none=True)

    @Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        guild = get_server(member.guild.id)
        if guild.voice_log_active:
            time = strftime("%d-%m-%Y %H:%M:%S", gmtime())
            timezone = datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo
            if not before.channel and after.channel:
                em = Embed(
                    title="Voice Log",
                    color=Color.green(),
                    description=f"Member {member.mention} joined voice channel {after.channel.mention} :inbox_tray:"
                )
                em.set_footer(text=f':clock1: {time} (Timezone: {timezone})')
                await member.guild.get_channel(channel_id=guild.voice_log_channel).send(embed=em)
            elif before.channel and not after.channel:
                em = Embed(
                    title="Voice Log",
                    color=Color.red(),
                    description=f"Member {member.mention} left voice channel {before.channel.mention} :outbox_tray:"
                )
                em.set_footer(text=f':clock1: {time} (Timezone: {timezone})')
                await member.guild.get_channel(channel_id=guild.voice_log_channel).send(embed=em)
            elif before.channel and after.channel:
                em = Embed(
                    title="Voice Log",
                    color=Color.gold(),
                    description=f"Member {member.mention} went from voice channel {before.channel.mention} to voice channel {after.channel.mention} :link:"
                )
                em.set_footer(text=f':clock1: {time} (Timezone: {timezone})')
                await member.guild.get_channel(channel_id=guild.voice_log_channel).send(embed=em)


def setup(bot: commands.Bot):
    bot.add_cog(SystemCog(bot))
