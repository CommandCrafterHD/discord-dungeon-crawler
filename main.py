import yaml
from discord import Message
from discord.ext import commands
from files.utility import get_server
from files import colors
from os import listdir

config = yaml.load(open("conf/config.yaml"), yaml.Loader)


def get_server_prefix(bot: commands.Bot, message: Message):
    if not message.guild:
        return config["prefix"]
    prefix = get_server(message.guild.id).prefix
    return commands.when_mentioned_or(prefix)(bot, message)


client: commands.bot = commands.AutoShardedBot(command_prefix=get_server_prefix)


MODULES = []
for i in listdir("modules"):
    if i.endswith('.py'):
        MODULES.append('modules.' + i[:-3])
    else:
        continue

client.remove_command('help')


print("--"*20)
print(f"{colors.YELLOW}LOADING MODULES!{colors.END}")
print("--"*20)
for module in MODULES:
    try:
        client.load_extension(module)
        print(f"{colors.CYAN}Module {module}{colors.END}: {colors.GREEN}LOADED{colors.END}")
    except Exception as e:
        print(f"{colors.CYAN}Module {module}{colors.END}: {colors.RED}NOT LOADED\n({e}){colors.END}")
print("--"*20)

# Running the bot
if __name__ == "__main__":
    client.run(config["token"])
